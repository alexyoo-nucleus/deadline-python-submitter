## Deadline Python Submitter
This program provides a GUI for users to submit Python jobs to a Deadline server.

## Description
Within a PySide2 window, users will be able to do the following:
- select a python file from their file system
- select the Deadline pool
- choose standard or maintenance mode
- select the specific render farm to submit to

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.
