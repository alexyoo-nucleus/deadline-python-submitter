import sys
from PySide2.QtWidgets import *
from PySide2.QtCore import *


class PythonJobSubmitter(QDialog):
    def __init__(self, parent=None) -> None:
        QDialog.__init__(self, parent=parent)
        self.setWindowTitle("Deadline Python Submitter")
        self.resize(500, 60)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        main_layout = QVBoxLayout()
        file_browser_label = QLabel("Select a Python script to send to Deadline")
        main_layout.addWidget(file_browser_label)

        # File Selection
        job_info_layout = QGridLayout()
        file_label = QLabel("File:")
        file_path = QLineEdit()
        file_path.setMinimumWidth(40)
        browse_btn = QPushButton("Browse")
        browse_btn.clicked.connect(self.browse_files)
        job_info_layout.addWidget(file_label, 0, 0)
        job_info_layout.addWidget(file_path, 0, 1)
        job_info_layout.addWidget(browse_btn, 0, 2)
        # Arguments
        args_label = QLabel("Arguments:")
        arguments = QLineEdit()
        job_info_layout.addWidget(args_label, 1, 0)
        job_info_layout.addWidget(arguments, 1, 1)
        # Python Version
        version_label = QLabel("Python Version:")
        select_python_version = QComboBox()
        select_python_version.setMaximumWidth(50)
        select_python_version.addItem("3.6")
        select_python_version.addItem("2.8")
        job_info_layout.addWidget(version_label, 2, 0)
        job_info_layout.addWidget(select_python_version, 2, 1)

        main_layout.addLayout(job_info_layout)
        self.setLayout(main_layout)

    def browse_files(self) -> None:
        """
        Allows user to browse their file system and select a .py file to submit. The
        QLineEdit text is updated with the file path if a file is selected.
        """
        picked_file = QFileDialog.getOpenFileName(
            self, "Select File", "C:\\", "Python Scripts (*.py)"
        )
        if picked_file[0] != "":
            self.file_path.setText(f"{picked_file[0]}")


def execute_python_job_submitter() -> None:
    """
    Execute the m
    """
    # Create application
    app = QApplication(sys.argv)
    # Create and Show the submitter
    sub = PythonJobSubmitter()
    sub.show()
    # Run main Qt loop
    sys.exit(app.exec_())


if __name__ == "__main__":
    execute_python_job_submitter()
